﻿//Подключение графической библиотеки
#include <SFML/Graphics.hpp>
#include <thread>
#include <chrono>

using namespace std::chrono_literals;

int main()
{
    sf::RenderWindow window(sf::VideoMode(800, 500), L"2 вариант");

    //1
    sf::CircleShape shape1(180.f);
    shape1.setFillColor(sf::Color(75,0,130,255 ));
    int shape1_x = 0, shape1_y = 25;
    shape1.setPosition(shape1_x, shape1_y);

    //2
    sf::CircleShape shape2(100.f);
    shape2.setFillColor(sf::Color(186,85,211,255));
    int shape2_x = 0, shape2_y = 200;
    shape2.setPosition(shape2_x, shape2_y);

    //3
    sf::CircleShape shape3(70.f);
    shape3.setFillColor(sf::Color(238,130,238,255));
    int shape3_x = 0, shape3_y = 310;
    shape3.setPosition(shape3_x, shape3_y);


    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
        }

        shape1_x+=3;
        if (shape1_x > 440)
            shape1_x = 440;
        shape1.setPosition(shape1_x, shape1_y);

        shape2_x+=5;
        if (shape2_x > 600)
            shape2_x = 600;
        shape2.setPosition(shape2_x, shape2_y);

        shape3_x += 6;
        if (shape3_x > 660)
            shape3_x = 660;
        shape3.setPosition(shape3_x, shape3_y);



        window.clear();
        window.draw(shape1);
        window.draw(shape2);
        window.draw(shape3);
        window.display();
        std::this_thread::sleep_for(40ms);
    }

    return 0;
}